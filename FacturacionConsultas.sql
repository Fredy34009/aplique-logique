SELECT * FROM facturacion.detalle;
use facturacion;

select detalle.num_detalle,count(detalle.id_producto)as cantidad,cliente.nombre  from detalle
inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto
inner join cliente on cliente.id_cliente=factura.id_cliente inner join categoria on producto.id_categoria=categoria.id_categoria where categoria.id_categoria=1
  group by detalle.num_factura order by cantidad desc;

select cliente.nombre,count(detalle.id_producto)as cantidad,if(count(categoria.id_categoria=1)>count(categoria.id_categoria>1),'Si','No') from detalle  
inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto
inner join cliente on cliente.id_cliente=factura.id_cliente inner join categoria on producto.id_categoria=categoria.id_categoria ;

select cliente.nombre,count(detalle.id_producto)as cantidad,IF(sum(detalle.id_producto=1)>sum(detalle.id_producto>1),'Si','No') from detalle  
inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto
inner join cliente on cliente.id_cliente=factura.id_cliente inner join categoria on producto.id_categoria=categoria.id_categoria

;
use facturacion;
select sum(precio) as precio from detalle inner join factura on factura.num_factura=detalle.num_factura where factura.fecha between '2019-11-01' and '2019-12-31';

select * from detalle inner join factura on factura.num_factura=detalle.num_factura inner join modo_pago on modo_pago.num_pago=factura.num_pago
inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria
inner join cliente on cliente.id_cliente=factura.id_cliente where factura.num_pago=3 and cliente.nombre like 'r_%' group by cliente.nombre;

select sum(detalle.precio) as precio,producto.nombre from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by producto.nombre;
select sum(detalle.precio)as precio,categoria.nombre from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by categoria.nombre;

select sum(detalle.precio)as precio,detalle.id_producto,detalle.cantidad,categoria.nombre,detalle.num_detalle,detalle.num_factura from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by categoria.nombre;



select detalle.num_detalle,detalle.num_factura,detalle.precio,detalle.id_producto,count(detalle.id_producto)as cantidad,cliente.nombre  from detalle 
inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto
inner join cliente on cliente.id_cliente=factura.id_cliente inner join categoria on producto.id_categoria=categoria.id_categoria where categoria.id_categoria=1 group by detalle.num_factura order by cantidad desc;


select detalle.id_producto,sum(detalle.precio)as precio,detalle.cantidad,categoria.nombre,detalle.num_detalle,detalle.num_factura,categoria.nombre from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by categoria.nombre;
package com.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.imp.DetalleImp;

@Controller
public class ClientesMb {

	ModelAndView mav = new ModelAndView();

	@RequestMapping(value = "/inicio", method = RequestMethod.GET)
	public ModelAndView bebidas()

	{
		mav.addObject("client", new DetalleImp().clientesBebidas());
		mav.addObject("costo", new DetalleImp().costos());
		mav.addObject("clienteR", new DetalleImp().clientesR());
		mav.addObject("ingresoProducto", new DetalleImp().ingresoProducto());
		mav.addObject("ingresoCategoria", new DetalleImp().ingresoCategoria());
		mav.setViewName("bebidas");
		return mav;
	}
}

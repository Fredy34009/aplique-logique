package com.config;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.imp.ClienteImp;

@Configuration
@EnableWebMvc
@ComponentScan("com")
public class Config {

	@Bean
	InternalResourceViewResolver viewRes() {
		InternalResourceViewResolver r = new InternalResourceViewResolver();
		r.setPrefix("/WEB-INF/vistas/");
		r.setSuffix(".jsp");
		return r;
	}

	@Bean
	public SessionFactory getConex() {
		return HibernateUtil.getSessionFactory();
	}

	@Bean
	public ClienteImp clientess() {
		return new ClienteImp();
	}
}

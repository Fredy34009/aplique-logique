package com.config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.models.Categoria;
import com.models.Cliente;
import com.models.Detalle;
import com.models.DetalleId;
import com.models.Factura;
import com.models.ModoPago;
import com.models.Producto;

public class HibernateUtil {
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/facturacion?useSSL=false");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
//				settings.put(Environment.URL, "jdbc:mysql://usam-sql.sv.cds:3306/facturacion?useSSL=false");
//				settings.put(Environment.USER, "kz");
//				settings.put(Environment.PASS, "kzroot");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				// settings.put(Environment.HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Categoria.class);
				configuration.addAnnotatedClass(Cliente.class);
				configuration.addAnnotatedClass(Detalle.class);
				configuration.addAnnotatedClass(DetalleId.class);
				configuration.addAnnotatedClass(Factura.class);
				configuration.addAnnotatedClass(ModoPago.class);
				configuration.addAnnotatedClass(Producto.class);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
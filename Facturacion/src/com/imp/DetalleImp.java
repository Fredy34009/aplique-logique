package com.imp;

import java.util.List;

import org.hibernate.Session;

import com.config.HibernateUtil;
import com.models.Detalle;

public class DetalleImp extends AbstractFacade<Detalle> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public DetalleImp() {
		super(Detalle.class);
		// TODO Auto-generated constructor stub
	}

	public List<Detalle> clientesBebidas() {
		session.beginTransaction();
		List<Detalle> lista = session.createNativeQuery(
				"select detalle.num_detalle,detalle.num_factura,detalle.precio,detalle.id_producto,count(detalle.id_producto)as cantidad,cliente.nombre  from detalle\r\n"
						+ "inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto\r\n"
						+ "inner join cliente on cliente.id_cliente=factura.id_cliente inner join categoria on producto.id_categoria=categoria.id_categoria where categoria.id_categoria=1\r\n"
						+ "  group by detalle.num_factura order by cantidad desc;",
				Detalle.class).getResultList();
		session.getTransaction().commit();

		for (Detalle det : lista) {
			System.out.println("Valor bebidas:  ****" + det.getCantidad());
			System.out.println("Valor bebidas: *****" + det.getFactura().getCliente().getNombre());
		}
		return lista;
	}

	// Muestra los costos de niviembre y diciembre del a�o 2019
	public List<Detalle> costos() {
		session.beginTransaction();
		List<Detalle> lista = session.createNativeQuery(
				"select detalle.id_producto,detalle.num_detalle,detalle.num_factura,detalle.cantidad, sum(detalle.precio) as precio from detalle inner join factura on factura.num_factura=detalle.num_factura where factura.fecha between '2019-11-01' and '2019-12-31'",
				Detalle.class).getResultList();
		session.getTransaction().commit();

		for (Detalle det : lista) {
			System.out.println("pRECIO DE NOVIEMBRE Y DICIEMBRE:  ****" + det.getPrecio());
		}
		return lista;
	}

	// Muestra todos los clientes que su nombre empieza por r
	public List<Detalle> clientesR() {
		session.beginTransaction();
		List<Detalle> lista = session.createNativeQuery(
				"select * from detalle inner join factura on factura.num_factura=detalle.num_factura inner join modo_pago on modo_pago.num_pago=factura.num_pago\r\n"
						+ "inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria\r\n"
						+ "inner join cliente on cliente.id_cliente=factura.id_cliente where factura.num_pago=3 and cliente.nombre like 'r_%' group by cliente.nombre;",
				Detalle.class).getResultList();
		session.getTransaction().commit();

		for (Detalle det : lista) {
			System.out.println("Valor 1:  ****" + det.getCantidad());
			System.out.println("Valor 2: *****" + det.getFactura().getCliente().getNombre());
		}
		return lista;
	}

	// ingresos por producto
	public List<Detalle> ingresoProducto() {
		session.beginTransaction();
		List<Detalle> lista = session.createNativeQuery(
				"select sum(detalle.precio) as precio,detalle.id_producto,detalle.cantidad,producto.nombre,detalle.num_detalle,detalle.num_factura from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by producto.nombre;",
				Detalle.class).getResultList();
		session.getTransaction().commit();

		for (Detalle det : lista) {
			System.out.println("Valor pRECIO:  ****" + det.getPrecio());
			System.out.println("Valor pRODUCTO: *****" + det.getProducto().getNombre());
		}
		return lista;
	}

	// ingresos por categoria
	public List<Detalle> ingresoCategoria() {
		session.beginTransaction();
		List<Detalle> lista = session.createNativeQuery(
				"select detalle.id_producto,sum(detalle.precio)as precio,detalle.cantidad,categoria.nombre,detalle.num_detalle,detalle.num_factura,categoria.nombre from detalle inner join factura on factura.num_factura=detalle.num_factura inner join producto on producto.id_producto=detalle.id_producto inner join categoria on categoria.id_categoria=producto.id_categoria group by categoria.nombre;",
				Detalle.class).getResultList();
		session.getTransaction().commit();

		for (Detalle det : lista) {
			System.out.println("cATEGORIA:  ****" + det.getProducto().getCategoria().getNombre());
			System.out.println("iNGRESO: *****" + det.getPrecio());
		}
		return lista;
	}
}

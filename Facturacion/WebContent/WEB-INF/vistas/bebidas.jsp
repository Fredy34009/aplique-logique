<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="${pageContext.request.servletContext.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.servletContext.contextPath}/resources/js/bootstrap.min.js">
	
</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-2">
				<h5>Top de clientes bebidas</h5>
				<table class="table table-dark">
					<thead>
						<tr>
							<td>Nombre</td>
							<td>Cantidad</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${client}" var="x">
							<tr>
								<td>${x.factura.cliente.nombre}</td>
								<td>${x.cantidad}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-2">
				<table class="table table-striped">
					<thead>
						<tr>
							<td>Costos Nov. y Dic</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${costo}" var="c">
							<tr>
								<td>${c.precio}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-2">
				<h6>Clientes que pagaron con tarjeta de regalo y su nombre empieza con r</h6>
				<table class="table table-dark">
					<thead>
						<tr>
							<td>Nombre</td>
							<td>Gasto</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${clienteR}" var="cr">
							<tr>
								<td>${cr.factura.cliente.nombre}</td>
								<td>${cr.precio}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-2">
				<h6>Ingreso por producto</h6>
				<table class="table table-striped">
					<thead>
						<tr>
							<td>Producto</td>
							<td>Ingreso</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ingresoProducto}" var="ip">
							<tr>
								<td>${ip.producto.nombre}</td>
								<td>${ip.precio}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-2">
				<h6>Ingresos Categoria</h6>
				<table class="table table-dark">
					<thead>
						<tr>
							<td>Categoria</td>
							<td>Ingreso</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${ingresoCategoria}" var="icat">
							<tr>
								<td>${icat.producto.categoria.nombre}</td>
								<td>${icat.precio}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
</body>
</html>
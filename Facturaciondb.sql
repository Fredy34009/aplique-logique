create database facturacion;
use facturacion;

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(60),
  PRIMARY KEY (`id_categoria`)
) ;

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `direccion` varchar(40) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ;


CREATE TABLE `modo_pago` (
  `num_pago` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `otros_detalles` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`num_pago`)
);


CREATE TABLE `factura` (
  `num_factura` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(7) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `num_pago` int(11) NOT NULL,
  PRIMARY KEY (`num_factura`),
  KEY `fk_cliente` (`id_cliente`),
  KEY `fk_modo_pago` (`num_pago`),
  CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `fk_modo_pago` FOREIGN KEY (`num_pago`) REFERENCES `modo_pago` (`num_pago`)
) ;

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `precio` double NOT NULL,
  `stock` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `fk_categoria` (`id_categoria`),
  CONSTRAINT `fk_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`)
) ;

CREATE TABLE `detalle` (
  `num_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `num_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double NOT NULL,
  PRIMARY KEY (`num_detalle`,`num_factura`),
  KEY `fk_factura` (`num_factura`),
  KEY `fk_producto` (`id_producto`),
  CONSTRAINT `fk_factura` FOREIGN KEY (`num_factura`) REFERENCES `factura` (`num_factura`),
  CONSTRAINT `fk_producto` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id_producto`)
) ;
